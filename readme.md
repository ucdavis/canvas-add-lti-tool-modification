# Canvas LTI +App Button Modifications

Modifies the default behavior of the course LTI tool addition button to redirect to website containing a request form.

## Installing

This code snippet can be uploaded as the javascript file for a custom Canvas theme or incorporated into a pre-exising custom javascript file.