/**
 * Canvas theme custom javascript.
 *
 * Modifies the behavior of the course level '+App' button for LTI tool additions.
 * The button text is changed to 'Request App' and creates a new browser window
 * which loads a LTI tool request form. This modification is applied for non-admin users
 *
 * @link   https://bitbucket.org/simon_dvorak/canvas-add-lti-tool-modification
 * @author Simon Dvorak.
 */

'use strict';

$(function(){
  var ltiRedirectURL = "https://movetocanvas.ucdavis.edu/requesting-lti-apps/";
  // create new MutationObserver object and pass callback to execute if mutation happens
  var observer = new MutationObserver(function(MutationRecord) {
    ltiRedirect(ltiRedirectURL);
  });
  // specify to watch for changes to the child list and include subtree nodes
  var config = {
      childList: true,
      subtree: true
  };
  // start watching the whole document
  observer.observe(document, config);
});


/**
 * Alter the function of the add LTI app button in a course
 */
function ltiRedirect(ltiRedirectURL){
    var add_tool_link = $(".add_tool_link");
    // If the add LTI App button exists, modify it for non admins
    // We look for the presence of an aria-label attribute with
    // the value of 'Add App'
    if(add_tool_link.attr("aria-label") == "Add App"){
      add_tool_link.css("display","block");
    if(add_tool_link.text() != "Request App" && $.inArray('admin',ENV['current_user_roles']) == -1){
      var redirect_url = ltiRedirectURL;
      add_tool_link.text("Request App");
      add_tool_link.attr("href",redirect_url);
      add_tool_link.attr("target","_blank");
      add_tool_link.attr("aria-label","Request App");
      add_tool_link.removeClass("icon-plus");
      // Removing the data-reactid attribute prevents the
      // default React function from firing
      add_tool_link.removeAttr("data-reactid");
    }
  }
}